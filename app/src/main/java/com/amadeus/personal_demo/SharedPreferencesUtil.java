package com.amadeus.personal_demo;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtil {
    private static final String NAME = "Config";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferencesUtil sharedPreferencesUtil;

    private SharedPreferencesUtil(){

    }

    public static SharedPreferencesUtil getInstantiation(Context context){
        if (sharedPreferences == null){
            sharedPreferencesUtil = new SharedPreferencesUtil();
            getSharedPreferences(context);
        }
        return sharedPreferencesUtil;
    }

    private static void getSharedPreferences(Context context){
        sharedPreferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
    }

    public void putInt(int value,String key){
        sharedPreferences.edit().putInt(key,value).apply();
    }
    public void putString(String value,String key){
        sharedPreferences.edit().putString(key,value).apply();
    }
    public void putBoolean(boolean value,String key){
        sharedPreferences.edit().putBoolean(key,value).apply();
    }
    public void putLong(Long value,String key){
        sharedPreferences.edit().putLong(key,value).apply();
    }
    public void putFloat(Float value,String key){
        sharedPreferences.edit().putFloat(key,value).apply();
    }

    public int getInt(int defaultValue,String key){
        return sharedPreferences.getInt(key,defaultValue);
    }
    public String getString(String defaultValue,String key){
        return sharedPreferences.getString(key,defaultValue);
    }
    public Boolean getBoolean(Boolean defaultValue,String key){
        return sharedPreferences.getBoolean(key,defaultValue);
    }
    public Long getLong(Long defaultValue,String key){
        return sharedPreferences.getLong(key,defaultValue);
    }
    public Float getFloat(Float defaultValue,String key){
        return sharedPreferences.getFloat(key,defaultValue);
    }

    public void clear(){
        sharedPreferences.edit().clear().apply();
    }
}
