package com.amadeus.personal_demo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class Settings extends AppCompatActivity {
    MainActivity mai = new MainActivity();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        //setLang();
        Button change = (Button)findViewById(R.id.change);
        Button usb = (Button)findViewById(R.id.UsbDevice_Conn);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder changes = new AlertDialog.Builder(Settings.this);
                changes.setTitle(R.string.toptitle);
                changes.setMessage(R.string.message);
                changes.setCancelable(false);
                changes.setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Locale getlocale = LanguageUtil.getAppLocale(App.getContext());
                        String getApplocale = LanguageUtil.getAppLanguage(App.getContext());
                        LanguageUtil.changeAppLanguage(App.getContext(),getlocale,true);
                        changeAppLanguage();

                    }
                });
                changes.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                changes.show();
            }
        });
        usb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnUsbConn(v);
            }
        });
    }

    public void btnUsbConn(View view){
        //USB连接方式
        startActivityForResult(new Intent(this,UsbDeviceList.class), Constant.USB_REQUEST_CODE);
    }

    //中英文切换
    public void setLang(){
        String language = LanguageUtil.getAppLanguage(App.getContext());
        if (language.equals("en")){
            setLocale(Locale.US);
        }else if (language.equals("zh")){
            setLocale(Locale.SIMPLIFIED_CHINESE);
        }
    }
    public void setLocale(Locale mlocale){
        Resources resources = getResources();
        DisplayMetrics ms = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();

        configuration.locale = mlocale;
        resources.updateConfiguration(configuration,ms);
        LanguageUtil.saveLanguageSetting(App.getContext(),mlocale);
    }
    public void changess(View view){
        changeAppLanguage();
    }
    public void changeAppLanguage(){
        String lang = LanguageUtil.getAppLanguage(App.getContext());
        if (lang.equals("zh")){
            setLocale(Locale.US);
        }else if (lang.equals("en")){
            setLocale(Locale.SIMPLIFIED_CHINESE);
        }
        recreate();
    }
}

