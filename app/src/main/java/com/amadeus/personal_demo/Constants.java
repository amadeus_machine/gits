package com.amadeus.personal_demo;

public class Constants {
    //录入
    public static final byte COM_UPWEIGHT = 0x01;
    public static final byte WEIGHT_NORMAL = 0;
    public static final byte WEIGHT_BEIZI = 1;
    public static final byte WEIGHT_ZUZHI = 2;
    public static final byte WEIGHT_FINAL = 3;

    public static final byte COM_KEYDOWN = 0x11;
    public static final byte COM_KEYUP =  0x12;
    public static final byte COM_STATUS = 0x03;
    public static final byte COM_GETWEIGHT = 0x4;
    public static final byte COM_KEYPRINT = 0x15;
    public static final byte COM_KEYSET = 0x16;

    public static final byte COM_STATUS_COMPLETE = 0x1;
    public static final byte COM_STATUS_AUTOOVER = 0x21;
    public static final byte C0M_STATUS_EME = 0x2;
    public static final byte C0M_STATUS_EMEOFF = 0x22;

    public static final byte COM_STATUS_CHECKOVER = 0x3;
    public static final byte COM_STATUS_NOBEIZI = 0x5;
    public static final byte COM_STATUS_NOZUZHI = 0x04;

    //android到控制板
    public static final byte COM_GETWEIGHTACK = 0x54;
    public static final byte COM_CHECKHX711 = 0x55;

    //状态
    public static final byte STATUS_IDLE = 0;
    public static final byte STATUS_AUTO = 1;
    public static final byte STATUS_ATUOOVER = 2;


    public static int byte2Int(byte[] bytes) {
        if (bytes.length < 4) {
            if (bytes.length != 0) {
                byte[] bytes1 = new byte[4];
                if (bytes.length == 3) {
                    bytes1[0] = 0x00;
                    bytes1[1] = bytes[0];
                    bytes1[2] = bytes[1];
                    bytes1[3] = bytes[2];
                } else if (bytes.length == 2) {
                    bytes1[0] = 0x00;
                    bytes1[1] = 0x00;
                    bytes1[2] = bytes[0];
                    bytes1[3] = bytes[1];
                } else if (bytes.length == 1) {
                    bytes1[0] = 0x00;
                    bytes1[1] = 0x00;
                    bytes1[2] = 0x00;
                    bytes1[3] = bytes[0];
                }

                return (bytes1[0] & 0xff) << 24
                        | (bytes1[1] & 0xff) << 16
                        | (bytes1[2] & 0xff) << 8
                        | (bytes1[3] & 0xff);
            } else {
                return 0;
            }
        }
        return (bytes[0] & 0xff) << 24
                | (bytes[1] & 0xff) << 16
                | (bytes[2] & 0xff) << 8
                | (bytes[3] & 0xff);
    }
}
