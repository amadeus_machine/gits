package com.amadeus.personal_demo;

import android.content.Context;
import android.content.SharedPreferences;

public class Language {
    private static String names = "sharedpreferences";

    public static void putdata(Context context,String keys,Object object){
        SharedPreferences sp = context.getSharedPreferences(names,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        if (object instanceof  String){
            editor.putString(keys,(String) object);
        }else if (object instanceof  Integer){
            editor.putInt(keys,(Integer) object);
        }else if (object instanceof Boolean){
            editor.putBoolean(keys,(Boolean) object);
        }else if (object instanceof Float){
            editor.putFloat(keys,(Float) object);
        }else if (object instanceof Long){
            editor.putLong(keys,(Long) object);
        }
        editor.apply();
    }

    public static Object get(Context context,String keyss, Object defaultObject){
        SharedPreferences sp = context.getSharedPreferences(names,Context.MODE_PRIVATE);

        if (defaultObject instanceof  String){
            return sp.getString(keyss,(String) defaultObject);
        }else if (defaultObject instanceof Integer){
            return sp.getInt(keyss,(Integer) defaultObject);
        }else if (defaultObject instanceof Float){
            return sp.getFloat(keyss,(Float) defaultObject);
        }else if (defaultObject instanceof Long){
            return sp.getLong(keyss,(Long) defaultObject);
        }else if (defaultObject instanceof  Boolean){
            return sp.getBoolean(keyss,(Boolean) defaultObject);
        }
        return null;
    }

}
