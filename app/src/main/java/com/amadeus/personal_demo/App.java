package com.amadeus.personal_demo;

import android.app.Application;
import android.content.Context;
import android.view.View;

public class App extends Application {

    private static Context mContext;


    public void onCreate(){
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static Context getContext(){
        return mContext;
    }
}
