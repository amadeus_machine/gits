package com.amadeus.personal_demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartBootComplete extends BroadcastReceiver {
    static final String action_boot ="android.intent.action.BOOT_COMPLETED";

    //开机自启动

    @Override
    public void onReceive(Context context, Intent intent){
        String action = intent.getAction();
        Intent i = new Intent(context,MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
        /*if(action.equals(action_boot)){
            Intent i = new Intent(context,MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }*/
    }
}
