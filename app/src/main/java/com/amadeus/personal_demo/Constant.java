package com.amadeus.personal_demo;

public class Constant {

    public static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    public static final int USB_REQUEST_CODE = 0x002;
    public static final int CONN_STATE_DISCONN = 0x007;
    public static final int MESSAGE_UPDATE_PARAMETER = 0x009;

}
