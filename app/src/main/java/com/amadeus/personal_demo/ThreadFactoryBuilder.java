package com.amadeus.personal_demo;

import java.util.concurrent.ThreadFactory;

public class ThreadFactoryBuilder implements ThreadFactory {
    private String name;
    private int counter;

    public ThreadFactoryBuilder(String name){
        this.name = name;
        counter = 1;
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r,name);
        thread.setName("ThreadFactoryBuilder_" + name + "_" +counter);
        return thread;
    }
}
