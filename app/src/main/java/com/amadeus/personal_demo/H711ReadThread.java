package com.amadeus.personal_demo;

import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android_serialport_api.SerialPort;

import static android.content.ContentValues.TAG;

public class H711ReadThread extends Thread {
    //电子秤端口
    //端口
    boolean flag = true;
    private static final String PORT = "/dev/ttyS3" ;
    private static int MAXBUFLEN = 100;
    private int recvIndex = 0;
    private int maxLen = 0;

    SerialPort mSerialPort;
    private InputStream mInputStream;
    private OutputStream mOutputStream;
    private Byte mRecvBuf[] = new Byte[MAXBUFLEN];

    H711ReadThread(){
        try{
            File com = new File(PORT);
            Log.e(TAG,"mSerialPort = new SerialPort(com,115200,0);");
            System.out.println("mSerialPort = new SerialPort(com,115200,0);");
            mSerialPort = new SerialPort(com,115200,0);
            mInputStream = mSerialPort.getInputStream();
            mOutputStream = mSerialPort.getOutputStream();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void Destroy(){
        flag = false;
        this.interrupt();
        mSerialPort = null;
        mInputStream = null;
    }

    @Override
    public void run() {
        super.run();
        while(flag){
            if (mInputStream == null){
                return;
            }
            try {
                if(mInputStream.available()>0){
                    byte[] buffer = new byte[1024];
                    int size;
                    size = mInputStream.read(buffer);
                    if (size > 0) {
                        //mDioFunc.debugmsg(pHexToString(buffer,size));
                        recvIndex = 0;
                        RecvData(buffer,size);
                    }
                    buffer = null;

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void RecvData(byte[] buf,int size){
        byte check;
        int i;
        for(i=0;i<size;i++){
            if(recvIndex == 0){
                if(buf[i]!=(byte)(0xAA)){
                    continue;
                };
            }else if(recvIndex == 1){
                if(buf[i]!=(byte)0xAB){
                    recvIndex = 0;
                    continue;
                }
            }else if(recvIndex == 2){
                maxLen = buf[i];
            }else{
                if(maxLen >= MAXBUFLEN-1){
                    recvIndex = 0;
                }else if(maxLen == recvIndex+1){
                    MainActivity.getInstance().MssPro(mRecvBuf,recvIndex);
                    recvIndex=0;
                }
            }
            mRecvBuf[recvIndex]=buf[i];
            recvIndex++;

        }
    }

    private byte CheckSum(byte[] dat, int num)
    {
        byte bTemp = 0, i;
        for(i = 0; i < num; i ++){
            bTemp ^= dat[i];
        }
        return bTemp;
    }

    public void SendData(byte cmd,byte value){
        byte buf[] = new byte[6];
        buf[0]=(byte)0xAA;
        buf[1]=(byte)0xAB;
        buf[2]=6;
        buf[3]=cmd;
        buf[4]=value;
        buf[5]=CheckSum(buf,5);
        if (mOutputStream != null) {
            try {
                mOutputStream.write(buf);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

}
