package com.amadeus.personal_demo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import java.util.HashMap;
import java.util.Iterator;

public class UsbDeviceList extends Activity {

    private static final String DEBUG_TAG = "DeviceListActivity";
    public static LinearLayout deviceNameLinearLayout;

    private ListView UsbDevice_List = null;
    private ArrayAdapter<String> mUsbDeviceArrayAdapter;
    public static final String USB_NAME = "usb_name";

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.dialog_usb_list);

        UsbDevice_List = (ListView)findViewById(R.id.UsbDevicelist);
        mUsbDeviceArrayAdapter = new ArrayAdapter<String>(this,R.layout.usbdevice_name_item);
        UsbDevice_List.setOnItemClickListener(mDeviceClickListener);
        UsbDevice_List.setAdapter(mUsbDeviceArrayAdapter);
        getUsbDeviceList();
    }

    void messageBox(String err) {
        Toast.makeText(getApplicationContext(),
                err, Toast.LENGTH_SHORT).show();
    }

    boolean checkUsbDevicePidVid(UsbDevice dev) {
        int pid = dev.getProductId();
        int vid = dev.getVendorId();
        return ((vid == 34918 && pid == 256) || (vid == 1137 && pid == 85)
                || (vid == 6790 && pid == 30084)
                || (vid == 26728 && pid == 256) || (vid == 26728 && pid == 512)
                || (vid == 26728 && pid == 256) || (vid == 26728 && pid == 768)
                || (vid == 26728 && pid == 1024) || (vid == 26728 && pid == 1280)
                || (vid == 26728 && pid == 1536));
    }

    public void getUsbDeviceList(){
        UsbManager manager = (UsbManager)getSystemService(Context.USB_SERVICE);
        //获取设备列表信息
        HashMap<String, UsbDevice> devices = manager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = devices.values().iterator();

        int count = devices.size();
        Log.d(DEBUG_TAG,"count" + count);
        if (count > 0){
            while (deviceIterator.hasNext()){
                UsbDevice device = deviceIterator.next();
                String devicename = device.getDeviceName();
                if (checkUsbDevicePidVid(device)){
                    mUsbDeviceArrayAdapter.add(devicename);
                }
                Intent intent = new Intent();
                intent.putExtra(devicename,device);
                setResult(Activity.RESULT_OK, intent);
            }
        }else {
            String noDevices = getResources().getText(R.string.none_usbdevice).toString();
            Log.d(DEBUG_TAG,"noDevices" + noDevices);
            mUsbDeviceArrayAdapter.add(noDevices);
        }
    }

    private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            // Cancel discovery because it's costly and we're about to connect
            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            String noDevices = getResources().getText(R.string.none_usbdevice).toString();
            if (!info.equals(noDevices)) {
                String address = info;
                // Create the result Intent and include the MAC address
                Intent intent = new Intent();
                intent.putExtra(USB_NAME, address);
                // Set result and finish this Activity
                setResult(Activity.RESULT_OK, intent);
            }
            finish();
        }
    };
}
